# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 15:00:04 2021

@author: User
"""

def load_dict_from_file(filetoopen):
    import json
    return json.load(open(filetoopen))



def plot_significance_bars(boxplot_1_x, boxplot_2_x, height, tick_length):
    
    import matplotlib.pyplot as plt
    
    plt.plot([boxplot_1_x, boxplot_1_x, boxplot_2_x, boxplot_2_x], 
                 [height - tick_length, height, height, height - tick_length], 
                 linewidth=1, 
                 color='k')


def across_conds_plotting(dict_struct, figsize = [6.4, 4.8]):
    
    import numpy as np
    import matplotlib.pyplot as plt
    
    boxprops = dict(linewidth=0.5, color='black')
    medianprops = dict(color='black')
    meanprops = dict(color='black')
    
    var_types = ['spike_number', 'firing_rate']
    var_names = ['Norm. spike number', 'Norm. firing rate']
    
    conds = ['added_mod', 'added_nonmod', 'control']
    
    for var_ind, var in enumerate(var_types):
        fig = plt.figure(figsize=figsize)
        ax = plt.axes()
    
        for cond_ind, cond in enumerate(conds):
            
            data = dict_struct[var][cond]
            cells_number = len(data)
            
            plt.boxplot(data, 
                positions = [cond_ind],
                showmeans=True,
                meanline=True,
                widths = 0.6,
                sym='',
                boxprops=boxprops, 
                medianprops=medianprops,
                meanprops=meanprops)
            
            plt.scatter(np.random.normal(cond_ind, 0.05, cells_number), 
                        data, 
                        s=8, 
                        c=np.arange(1,cells_number*2,2), 
                        cmap='rainbow')
        
        # plot significance
        
        y_max = max(max(dict_struct[var][conds[0]]),
                    max(dict_struct[var][conds[1]]),
                    max(dict_struct[var][conds[2]]))
        y_min = min(min(dict_struct[var][conds[0]]),
                    min(dict_struct[var][conds[1]]),
                    min(dict_struct[var][conds[2]]))
        
        y_range = y_max - y_min
                  
        tick_length = y_range / 42
        
        height = y_max + tick_length*2
        
        plot_significance_bars(0, 1, height, tick_length)
        plt.text(0.5, height+tick_length/2, '* *', fontsize=figsize[1]*2)
        
        plot_significance_bars(0, 2, height + tick_length*4, tick_length)
        plt.text(1, height+tick_length*4.5, '* *', fontsize=figsize[1]*2)
        
        plt.ylabel(var_names[var_ind])
        ax.set_xticks(np.arange(len(conds)))
        ax.set_xticklabels(conds)

        plt.show()



data = load_dict_from_file("allcellsaverages.json")
across_conds_plotting(data)
