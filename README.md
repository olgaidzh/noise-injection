<i>allcellsaverages.json</i> contains the final results (average values for each cell)
<br>
<i>load_and_plot.py</i> plots these as boxplots, considering stat test results
<br>
<br>
<i>allcellsdata.json</i> contain overall information about cells, recording files and spike numbers and firing rates for each file
<br>
<i>gamma_mod.py</i> has all the latest function implementations to process recording files
<br>
<br>
<i>rec_bursts_batch_fun.py</i> is an old version of gamma_mod.py, not relevant anymore
