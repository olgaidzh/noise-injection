# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 10:45:28 2021

@author: User
"""

S_RATE = 20000  # Hz

SINE_FREQ = 1 # Hz


foldername = "C:\\Users\\User\\Documents\\Python Scripts\\noise_injection\\"

'''
All cells data
See 'allcellsdata.json'

9 cells in total

'date_cell':

'lowDC' and 'high DC': 
Files recorded with different holding currents
Not all cells have 'high DC's
We only make use of 'low DC's

'spike_numbers' and 'firing_rates':
Two lists, where each item corresponds to a recording file.
Bursts data computed from every file is a dict.
It is divided into three groups: 
'added_mod', 'added_nonmod' and 'control' 
and normalized to the overall mean value within each file

'aver_spike_numbers' and 'aver_firing_rates':
Two dicts that store three lists each, for the three groups 
('added_mod', 'added_nonmod' and 'control').
In the lists, each item is an average value within a recording file.
'''

all_cells_data = {
    '21-04-23_cell01':
        {
            'lowDC':
                [
                    "21423033.abf",
                    #"21423034.abf",
                    "21423036.abf",
                    "21423038.abf",
                    "21423040.abf",
                    "21423042.abf",
                    "21423045.abf",
                    "21423047.abf",
                    "21423049.abf"
                ],
            'spike_numbers': [],
            'firing_rates': [],
            
            'aver_spike_numbers': {'added_mod': [], 'control': [], 'added_nonmod': []},
            'aver_firing_rates': {'added_mod': [], 'control': [], 'added_nonmod': []}
        },
    '21-04-30_cell02':
        {
            'lowDC':
                [
                    "21430019.abf",
                    "21430023.abf",
                    "21430027.abf",
                    "21430033.abf",
                    "21430020.abf",
                    "21430024.abf",
                    "21430028.abf",
                    "21430034.abf"
                ],
            'highDC':
                [
                    "21430020.abf",
                    "21430024.abf",
                    "21430028.abf",
                    "21430034.abf"
                ],
            'spike_numbers': [],
            'firing_rates': [],
            
            'aver_spike_numbers': {'added_mod': [], 'control': [], 'added_nonmod': []},
            'aver_firing_rates': {'added_mod': [], 'control': [], 'added_nonmod': []}
        },
    '21-04-30_cell04':
        {
            'lowDC':
                [
                    "21430043.abf",
                    "21430047.abf",
                    "21430051.abf",
                    "21430055.abf",
                    "21430059.abf"
                ],
            'highDC':
                [
                    "21430044.abf",
                    "21430048.abf",
                    "21430052.abf",
                    "21430056.abf",
                    "21430061.abf"
                ],
            'spike_numbers': [],
            'firing_rates': [],
            
            'aver_spike_numbers': {'added_mod': [], 'control': [], 'added_nonmod': []},
            'aver_firing_rates': {'added_mod': [], 'control': [], 'added_nonmod': []}
        },
    '21-05-06_cell01':
        {
            'lowDC':
                [
                    "21506004.abf",
                    "21506009.abf",
                    "21506013.abf",
                    "21506017.abf"
                ],
            'highDC':
                [
                    "21506006.abf",
                    "21506010.abf",
                    "21506014.abf",
                    "21506018.abf"
                ],
            'spike_numbers': [],
            'firing_rates': [],
            
            'aver_spike_numbers': {'added_mod': [], 'control': [], 'added_nonmod': []},
            'aver_firing_rates': {'added_mod': [], 'control': [], 'added_nonmod': []}
        },
    '21-05-07_cell01':
        {
            'lowDC':
                [
                    "21507002.abf",
                    "21507008.abf",
                    "21507010.abf",
                    "21507015.abf"
                ],
            'highDC':
                [
                    "21507004.abf",
                    "21507006.abf",
                    "21507012.abf",
                    "21507018.abf"
                ],
            'spike_numbers': [],
            'firing_rates': [],
            
            'aver_spike_numbers': {'added_mod': [], 'control': [], 'added_nonmod': []},
            'aver_firing_rates': {'added_mod': [], 'control': [], 'added_nonmod': []}
        },
    '21-06-01_cell01':
        {
            'lowDC':
                [
                    "21601000.abf",
                    "21601002.abf",
                    "21601003.abf", # 10 mV
                    "21601004.abf",
                    "21601010.abf" # 20 mV
                ],
            'spike_numbers': [],
            'firing_rates': [],
            
            'aver_spike_numbers': {'added_mod': [], 'control': [], 'added_nonmod': []},
            'aver_firing_rates': {'added_mod': [], 'control': [], 'added_nonmod': []}
        },
    '21-06-01_cell02':
        {
            'lowDC':
                [
                    "21601011.abf", 
                    "21601012.abf",
                    "21601013.abf",
                    "21601014.abf",
                    "21601015.abf"
                ],
            'spike_numbers': [],
            'firing_rates': [],
            
            'aver_spike_numbers': {'added_mod': [], 'control': [], 'added_nonmod': []},
            'aver_firing_rates': {'added_mod': [], 'control': [], 'added_nonmod': []}
        },
    '21-06-07_cell01':
        {
            'lowDC':
                [
                    "21607000.abf", # or 05
                    "21607001.abf", # or 06
                    "21607002.abf", # or 07
                    "21607003.abf", # or 08
                    "21607004.abf", # or 09
                    "21607005.abf",
                    "21607006.abf",
                    "21607007.abf",
                    "21607008.abf",
                    "21607009.abf"
                ],
            'spike_numbers': [],
            'firing_rates': [],
            
            'aver_spike_numbers': {'added_mod': [], 'control': [], 'added_nonmod': []},
            'aver_firing_rates': {'added_mod': [], 'control': [], 'added_nonmod': []}
        },
    '21-06-07_cell02':
        {
            'lowDC':
                [
                    "21607011.abf", # or 18
                    "21607012.abf", # or 19
                    "21607013.abf", # or 20
                    "21607014.abf", # or 21
                    "21607015.abf", # or 22
                    "21607018.abf",
                    "21607019.abf",
                    "21607020.abf",
                    "21607021.abf",
                    "21607022.abf"
                ],
            'spike_numbers': [],
            'firing_rates': [],
            
            'aver_spike_numbers': {'added_mod': [], 'control': [], 'added_nonmod': []},
            'aver_firing_rates': {'added_mod': [], 'control': [], 'added_nonmod': []}
        }
}    


'''
All cells averages
See 'allcellsaverages.json'

'spike_number' and 'firing_rate':
Two dicts that store three lists, for three conditions
('added_mod', 'added_nonmod' and 'control')
In the lists, an item corresponds to a cell,
it is a single average value for the cell, computed across all its files.

'''
    
all_cells_averages = {
    'spike_number':
        {'added_mod': [], 'control': [], 'added_nonmod': []},
    'firing_rate':
        {'added_mod': [], 'control': [], 'added_nonmod': []}
    }

    
'''  
 In the cell response, cut off all the low frequences 
 to only keep the spikes
'''  
def filter_spikes(recording):
    # recording: IN mV
    from scipy.signal import butter
    from scipy.signal import filtfilt
    
    b, a = butter(3, 0.1, 'highpass')
    spikes_filtered = filtfilt(b, a, recording)
 
    '''
    import matplotlib.pyplot as plt
    fig1 = plt.figure()
    plt.plot(time, spikes_filtered)
    
    plt.show()
    '''
    return spikes_filtered


'''
Find spike times, mark them on the timeline
'''
def mark_spikes(recording, threshold = 6, width = 100):
    
    import numpy as np
    from scipy.signal import find_peaks
    
    filtered = filter_spikes(recording)
    rms = np.sqrt(np.mean(filtered**2))
    spike_times, _ = find_peaks(filtered, height = rms * threshold, distance = width)
    
    spikes = np.zeros(len(recording))
    for t in spike_times:
        spikes[t] = 1
    
    return spikes



'''
Extract individual bursts' coordinates
(A burst is a response to a single stimulus)

First, in the stimulus sequence, find all the sine minima, as borderlines between 
individual stimuli.
This function will be applied to the filtered stimulus 
(with all the higher frequency content removed, but the carrier sine)

Returns list of minima time coordinates and values
'''
def sine_analysis(data, time, SINE_FREQ = 1, stim_start = 1):
    import numpy as np
    
    from numpy import diff
    dx = time[1] - time[0] # const
    dy = diff(data[0: int(46.1*S_RATE)])/dx
    
    mindy = 0.00001 / dx
    
    extrms = []
    extr_t = []
    extr_y = []
    prev_t_mark = 0
    for t_mark, der_val in enumerate(dy):
        if der_val <= mindy and der_val >= -mindy and t_mark - prev_t_mark > 1/4/SINE_FREQ*S_RATE:
            extrms.append([t_mark, data[t_mark]])
            extr_t.append(t_mark)
            extr_y.append(data[t_mark])
            prev_t_mark = t_mark

    # extr_y are all extrema found, not just extrema of the sine! The latter will be extracted as located above the meanline of the sine

    mini_t = []
    mini_y = []
    
    maxi_t = []
    maxi_y = []
    
    mini_t.append(stim_start*S_RATE)
    mini_y.append(data[stim_start*S_RATE])
    
    n_extrs = len(extr_t)
    
    meanline = np.mean(extr_y)
    for ind, extr in enumerate(extr_y):
        '''
        if ind < n_extrs - 1:
            meanline = (extr + extr_y[ind+1]) / 2
        else:
            meanline = (extr + extr_y[ind-1]) / 2
        '''
        if extr > meanline:
            maxi_t.append(extr_t[ind])
            maxi_y.append(extr)
            
            if n_extrs > ind + 1:
                mini_t.append(extr_t[ind+1])
                mini_y.append(extr_y[ind+1])
    
    
    #mini_t.append(int(maxi_t[0] - 1/2/SINE_FREQ*S_RATE))
    #mini_y.append(data[int(maxi_t[0] - 1/2/SINE_FREQ*S_RATE)])
    
    return (mini_t, mini_y)
    
    
'''
Now, subdivide all the found spikes into bursts,
and compute spike number and firing rate within each burst
Stimulus is subdivided into a sequence of periods, 
i.e. individual stimuli presentations.
'''
def detect_bursts(stimdata, respdata, timeline, SINE_FREQ=1):
    
    from scipy.ndimage.filters import gaussian_filter1d
    from math import pi
    
    
    GAUSS_CUT_OFF_F = 4 * SINE_FREQ
    Gauss_sigma = int(S_RATE / 2 / pi // GAUSS_CUT_OFF_F) 
    IN1_pA_filtered = gaussian_filter1d(stimdata, Gauss_sigma)
    
    minima_t, minima_y = sine_analysis(IN1_pA_filtered, timeline)
    period_starts = []
    period_ends = []
    
    for i in range(len(minima_t)-1):
        period_starts.append(minima_t[i])
        period_ends.append(minima_t[i+1])
    
    
    '''
    import matplotlib.pyplot as plt
    fig1 = plt.figure()
    plt.plot(time, IN1_pA_filtered)
    for t in range(len(minima_t)):
        plt.scatter(minima_t[t]/S_RATE, minima_y[t])
    plt.show()
    '''
    
    # for each burst, calculate its spike number and firing rate
    
    spikes = mark_spikes(respdata)
    
    total_period_number = len(period_starts)
    
    periods_spike_nums = []
    periods_FR = []
    
    
    for period_num in range(total_period_number):
        
        period_spike_coords = [t for t in range(period_starts[period_num], period_ends[period_num]) if spikes[t]>0]
        
        period_spike_num = len(period_spike_coords)
        periods_spike_nums.append(period_spike_num)
        
        if period_spike_num > 0:
        
            first_spike_coord = period_spike_coords[0]
            last_spike_coord = period_spike_coords[-1]
        
            # period_length = period_ends[period_num] - period_starts[period_num]
            period_length = last_spike_coord - first_spike_coord
            
        
            if period_length != 0:
                periods_FR.append(period_spike_num / period_length * S_RATE)
            else:
                periods_FR.append(0)
        else:
            periods_FR.append(0)
    
    return (total_period_number, periods_spike_nums, periods_FR)


'''
A helper function to check if cell activity within a file 
declines significantly with time
'''
def cell_condition_check(sp_nums, filename):
    
    '''
    _, sp_nums, _ = detect_bursts_2()
    
    import matplotlib.pyplot as plt
    fig2 = plt.figure()
    fig2.suptitle(str('File: ' + str(filename)))
    plt.plot(sp_nums)
    plt.xlabel('presentation no.')
    plt.ylabel('spike number')
    plt.show()
    '''
    
    import matplotlib.pyplot as plt
    fig3 = plt.figure()
    fig3.suptitle(str('File: ' + str(filename)))
    for cond in sp_nums.keys():
        plt.plot(sp_nums[cond], label=cond)
    plt.xlabel('presentation no.')
    plt.ylabel('spike number')
    plt.legend()
    plt.show()



'''
Pool all the burst data extracted from a file into three groups,
for three conditions (added mod, added nonmod and control)
'''
def group_bursts(total_period_number, periods_spike_nums, periods_FR):
    spike_nums = {'added_mod': [], 'control': [], 'added_nonmod': []}
    firing_rates = {'added_mod': [], 'control': [], 'added_nonmod': []}
    
    period_num = 0
    
    # we exclude the last three presentations where there are two controls out of three
    while period_num < total_period_number - 3:
        # exclude all those triplet groups where at least once out of three no spikes were evoked
        if periods_spike_nums[period_num] > 0 and periods_spike_nums[period_num+1] > 0 and periods_spike_nums[period_num+2] > 0:
            spike_nums['added_mod'].append(periods_spike_nums[period_num])
            spike_nums['control'].append(periods_spike_nums[period_num+1])
            spike_nums['added_nonmod'].append(periods_spike_nums[period_num+2])
            
        # exclude single-spike bursts when calculating firing rate
            if (periods_spike_nums[period_num]!=1 and periods_spike_nums[period_num+1]!=1 and periods_spike_nums[period_num+2]!=1):
                firing_rates['added_mod'].append(periods_FR[period_num])
                firing_rates['control'].append(periods_FR[period_num+1])
                firing_rates['added_nonmod'].append(periods_FR[period_num+2])
                       
        period_num += 3
    
    return (spike_nums, firing_rates)




'''
Optionally, normalize data (spike number and firing rate) 
to the overall mean value within the file
'''   
def normalize_to_mean(spike_nums, firing_rates):
    
    import numpy as np
    
    # mean across all three stimulus types
    mean_spike_num = np.mean([spike_nums[cond] for cond in spike_nums.keys()])
    mean_firing_rate = np.mean([firing_rates[cond] for cond in firing_rates.keys()])
    
    norm_spike_nums = {'added_mod': [], 'control': [], 'added_nonmod': []}
    norm_firing_rates = {'added_mod': [], 'control': [], 'added_nonmod': []}
    
    for cond in spike_nums.keys():
        norm_spike_nums[cond] = list(np.array(spike_nums[cond]) / mean_spike_num)
        norm_firing_rates[cond] = list(np.array(firing_rates[cond]) / mean_firing_rate)
    
    return (norm_spike_nums, norm_firing_rates)


'''
For a dictionary of lists, a function to compute and return
a dictionary of average values for each list
'''
def calc_averages(dict_struct):
    dict_of_averages = dict()
    for key in dict_struct.keys():
        dict_of_averages[key] = sum(dict_struct[key]) / len(dict_struct[key])
    return dict_of_averages
    

'''
Main function to process all cells data 
and fill in 'all_cells_data' and 'all_cells_averages'
'''
def proc_all_cells_data():
    
    import pyabf
    
    for cell in all_cells_data.keys():
        print(cell)
        for filename in all_cells_data[cell]['lowDC']:
            print(filename)
            path = foldername + filename
            fileabf = pyabf.ABF(path)
    
            fileabf.setSweep(sweepNumber=0, channel=fileabf.adcUnits.index('mV'))
            IN0_mV = fileabf.sweepY
            
            fileabf.setSweep(sweepNumber=0, channel=fileabf.adcUnits.index('pA'))
            IN1_pA = fileabf.sweepY
            
            time = fileabf.sweepX
            
            (total_period_number, periods_spike_nums, periods_FR) = detect_bursts(IN1_pA, IN0_mV, time)    
            (spike_nums, firing_rates) = group_bursts(total_period_number, periods_spike_nums, periods_FR)
            (norm_spike_nums, norm_firing_rates) = normalize_to_mean(spike_nums, firing_rates)
            
            all_cells_data[cell]['spike_numbers'].append(norm_spike_nums)
            all_cells_data[cell]['firing_rates'].append(norm_firing_rates)
            
            # average values for each file
            averages = calc_averages(norm_spike_nums)
            for cond in norm_spike_nums.keys(): 
                all_cells_data[cell]['aver_spike_numbers'][cond].append(averages[cond])
                
            averages = calc_averages(norm_firing_rates)
            for cond in norm_spike_nums.keys():
                all_cells_data[cell]['aver_firing_rates'][cond].append(averages[cond])
            
            #cell_condition_check(spike_nums, filename)
            
        # average value across all files
        average = calc_averages(all_cells_data[cell]['aver_spike_numbers'])
        all_cells_data[cell]['aver_spike_number'] = average
            
        # add this average to a general dict for all cells
        for cond in norm_spike_nums.keys():
            all_cells_averages['spike_number'][cond].append(average[cond])
            
                
        average = calc_averages(all_cells_data[cell]['aver_firing_rates'])
        all_cells_data[cell]['aver_firing_rate'] = average
            
        for cond in norm_spike_nums.keys():
            all_cells_averages['firing_rate'][cond].append(average[cond])
            
            
'''
Two functions to perform dictionaries' import and export
'''
def save_dict_to_file(dictdata, filetocreate):
    import json
    json.dump(dictdata, open(filetocreate, 'w'))


def load_dict_from_file(filetoopen):
    import json
    return json.load(open(filetoopen))

# data = load_dict_from_file("allcellsdata.json")


'''
To look at the distributions within cells.
Plots cells*conditions=27 boxplots, one for each cell and each condition
Boxplots order is 'mod'-'nonmod'-'control' for each cell
'''
def single_cell_plotting():
    
    import numpy as np
    import matplotlib.pyplot as plt
    
    fig = plt.figure()
    ax = plt.axes()

    boxprops = dict(linewidth=0.5, color='black')
    medianprops = dict(color='black')
    
    count=0
    for cell in all_cells_data.keys():
        count+=1
        
        #plot for single condition (mix type)
        '''
        cond ='control'
        datalist = all_cells_data[cell]['aver_spike_numbers'][cond]
        xposition = count
        plt.boxplot(datalist, 
                        positions = [xposition], 
                        widths = 0.3,
                        sym='',
                        boxprops=boxprops, 
                        medianprops=medianprops)
        plt.scatter(np.random.normal(xposition, 0.05, len(datalist)), datalist, s=5)
        '''
        
        # plot for all three conditions
        for ind, cond in enumerate(['added_mod', 'added_nonmod', 'control']):
            datalist = all_cells_data[cell]['aver_firing_rates'][cond]
            xposition = count+0.3*ind
            plt.boxplot(datalist, 
                        positions = [xposition], 
                        widths = 0.3,
                        sym='',
                        boxprops=boxprops, 
                        medianprops=medianprops)
            plt.ylabel('Norm. firing rate')
            plt.scatter(np.random.normal(xposition, 0.05, len(datalist)), datalist, s=5)
        
        
    plt.show()


'''
Main plotting function

Three boxplots for three conditions, 
each is a distribution of average values across all cells
solid lines are medians,
dashed lines are mean values
'''
def across_conds_plotting(dict_struct):
    
    import numpy as np
    import matplotlib.pyplot as plt
    
    boxprops = dict(linewidth=0.5, color='black')
    medianprops = dict(color='black')
    meanprops = dict(color='black')
    
    var_types = ['spike_number', 'firing_rate']
    var_names = ['Norm. spike number', 'Norm. firing rate']
    
    conds = ['added_mod', 'added_nonmod', 'control']
    
    for var_ind, var in enumerate(var_types):
        fig = plt.figure()
        ax = plt.axes()
    
        for cond_ind, cond in enumerate(conds):
            
            data = dict_struct[var][cond]
            cells_number = len(data)
            
            plt.boxplot(data, 
                positions = [cond_ind],
                showmeans=True,
                meanline=True,
                widths = 0.6,
                sym='',
                boxprops=boxprops, 
                medianprops=medianprops,
                meanprops=meanprops)
            
            plt.scatter(np.random.normal(cond_ind, 0.05, cells_number), 
                        data, 
                        s=8, 
                        c=np.arange(1,cells_number*2,2), 
                        cmap='rainbow')
            
        plt.ylabel(var_names[var_ind])
        ax.set_xticks(np.arange(len(conds)))
        ax.set_xticklabels(conds)

        plt.show()



'''
Shapiro-Wilk test for normality for the above three distributions
'''
def normality_test(dict_struct):
    
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import shapiro
    from statsmodels.graphics.gofplots import qqplot
    
    for var in dict_struct.keys():
        print('    ', var)
        for key in dict_struct[var].keys():
            print('  ', key)
            shap = shapiro(dict_struct[var][key])
            print(shap)
            if shap.pvalue>0.01:
                print('H0 can not be rejected')
            else:
                print('H0 is rejected')
            fig = plt.figure()
            ax=plt.axes()
            fig.suptitle(var+' '+ key)
            qqplot(np.array(dict_struct[var][key]), fit=True, line='45', ax=ax)
            plt.show()
        print('* * *')
    


'''
Levene's test for equal variances
(to make sure anova is applicable)
'''
def test_for_equal_variances(dict_struct):
    from scipy.stats import levene
    for var in dict_struct.keys():
        print('    ', var)
        lev = levene(dict_struct[var]['added_mod'], dict_struct[var]['control'], center='median')       
        print(lev)
        if lev.pvalue>0.01:
            print('H0 can not be rejected')
        else:
            print('H0 is rejected')
            
        lev = levene(dict_struct[var]['added_nonmod'], dict_struct[var]['control'], center='median')       
        print(lev)
        if lev.pvalue>0.01:
            print('H0 can not be rejected')
        else:
            print('H0 is rejected')
        print('* * *')


'''
Anova (optionally, exclude one of the conditions from the test)

p-values (spike number; firing rate):
'Mod' and 'control': 
4.2e-06; 1.1e-05 
'nonmod' and 'control': 
0.016; 0.884
'mod' and 'nonmod': 
0.00031; 0.00016
'''
def one_way_anova(dict_struct):
    from scipy.stats import f_oneway
    for var in dict_struct.keys():
        print('    ', var)
        anovares = f_oneway(dict_struct[var]['added_mod'], 
                             dict_struct[var]['added_nonmod'],
                             dict_struct[var]['control'])
        print(anovares)
        if anovares.pvalue>0.01:
            print('H0 can not be rejected')
        else:
            print('H0 is rejected')
        print('* * *')



proc_all_cells_data()

#save_dict_to_file(all_cells_data, "allcellsdata.json")
#save_dict_to_file(all_cells_averages, "allcellsaverages.json")

#single_cell_plotting()
#across_conds_plotting(all_cells_averages)

#normality_test(all_cells_averages)
#test_for_equal_variances(all_cells_averages)
one_way_anova(all_cells_averages)
