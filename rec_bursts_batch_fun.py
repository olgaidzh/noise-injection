# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 12:34:28 2020

@author: User
"""

S_RATE = 20000  # Hz
mix_type = 'beta'  # 'beta' or 'gamma'

#load source abfs

foldername = "C:\\Users\\User\\Documents\\Python Scripts\\noise_injection\\"

if mix_type == 'beta':
    '''
    files = [
        "21430017.abf",
        "21430021.abf",
        "21430029.abf",
        "21430031.abf"
        ]
    '''
    files = [
        "21430018.abf",
        "21430022.abf",
        "21430030.abf",
        "21430032.abf"        
        ]
    
elif mix_type == 'gamma':
    '''
    files = [
        "21430019.abf",
        "21430023.abf",
        "21430027.abf",
        "21430033.abf"
        ]
    '''
    
    files = [
        "21430020.abf",
        "21430024.abf",
        "21430028.abf",
        "21430034.abf"
        ]



# recording is a time series of fileabf.sweep (mV)
# SPIKE_DURATION [samples]
# SPIKE_THRESHOLD [mV]
# returns a list of spike coordinates [seconds]

def mark_spikes(recording, SPIKE_DURATION = 100, SPIKE_THRESHOLD = 0):

    filelen = len(recording)
    
    #spikes = np.zeros(len(recording))
    spike_coords = []
    
    time = 0
    fl = 1
    while time < filelen:
        if recording[time] > SPIKE_THRESHOLD:
            #spikes[time] = 1
            spike_coords.append(time/S_RATE)
            fl = 0
        if fl == 0:
            time += SPIKE_DURATION
            fl = 1
        else:
            time += 1    
    return spike_coords




# extract individual bursts' coordinates

# bursts_starts, for each burst, stores the coordinate of its first spike
# bursts_ends, the same, the last spike of each burst
# SINE_FREQ [Hz]

def detect_bursts(spike_coords, SINE_FREQ = 2):
    BORDERLINE = 1/SINE_FREQ # seconds
    
    total_spike_number = len(spike_coords)
    
    bursts_starts = dict()
    bursts_ends = dict()
    spike_num = 0
    last_num = 0
    while spike_num < total_spike_number - 1:
        neighbour_num = spike_num + 1
        while spike_coords[neighbour_num] - spike_coords[neighbour_num - 1] < BORDERLINE and neighbour_num < total_spike_number - 1:
            neighbour_num += 1 
        bursts_starts[spike_num] = spike_coords[spike_num]
        bursts_ends[spike_num] = spike_coords[neighbour_num - 1]
        last_num = spike_num
        spike_num = neighbour_num

    if spike_coords[neighbour_num] - spike_coords[neighbour_num - 1] < BORDERLINE:
        bursts_ends[last_num] = spike_coords[neighbour_num]
    else:
        bursts_starts[spike_num] = spike_coords[spike_num]
        bursts_ends[spike_num] = spike_coords[spike_num]


    bursts_starts_listed = np.array(list(bursts_starts.items()))
    bursts_starts_listed = bursts_starts_listed[np.argsort(bursts_starts_listed[:,0])]

    bursts_ends_listed = np.array(list(bursts_ends.items()))
    bursts_ends_listed = bursts_ends_listed[np.argsort(bursts_ends_listed[:,0])]


    # for each burst, calculate its spike number and firing rate
    
    total_burst_number = len(bursts_starts)
    
    bursts_spike_nums = []
    bursts_FR = []
    
    for num, leading_spike_num in enumerate(bursts_starts_listed[:,0]):
        if num < total_burst_number - 1:
            next_leading = bursts_starts_listed[num+1][0]
            burst_length = bursts_ends_listed[num][1] - bursts_starts_listed[num][1]
            bursts_spike_nums.append(next_leading - leading_spike_num)
            if burst_length != 0:
                bursts_FR.append((next_leading - leading_spike_num) / burst_length)
            else:
                bursts_FR.append(0)
    return (total_burst_number, bursts_spike_nums, bursts_FR)




# pool all the data from a single file together in three groups
def group_bursts(total_burst_number, bursts_spike_nums, bursts_FR):
    
    mod_added_spike_nums = []
    mod_added_FR = []
    
    nothing_added_spike_nums = []
    nothing_added_FR = []
    
    nonmod_added_spike_nums = []
    nonmod_added_FR = []
    
    burst_num = 0
    while burst_num < total_burst_number - 3:
         # exclude single-spike bursts when calculating firing rate
        if (bursts_spike_nums[burst_num]!=1 and bursts_spike_nums[burst_num+1]!=1 and bursts_spike_nums[burst_num+2]!=1):
            mod_added_FR.append(bursts_FR[burst_num])
            nothing_added_FR.append(bursts_FR[burst_num+1])
            nonmod_added_FR.append(bursts_FR[burst_num+2])
        burst_num += 3
    
    burst_num = 0
    while burst_num < total_burst_number - 3:
        mod_added_spike_nums.append(bursts_spike_nums[burst_num])
        nothing_added_spike_nums.append(bursts_spike_nums[burst_num+1])
        nonmod_added_spike_nums.append(bursts_spike_nums[burst_num+2])
        
        burst_num += 3
    
    import pandas as pd
    spike_nums = pd.DataFrame(list(zip(mod_added_spike_nums, nothing_added_spike_nums, nonmod_added_spike_nums)), columns=['Mod','Control','Nonmod'])
    firing_rates = pd.DataFrame(list(zip(mod_added_FR, nothing_added_FR, nonmod_added_FR)), columns=['Mod','Control','Nonmod'])

    return (spike_nums, firing_rates)



# optionally, normalize data (spike number and firing rate) to the mean value within the file 
   
def normalize_to_mean(bursts_spike_nums, bursts_FR, spike_nums, firing_rates):
    
    import numpy as np
    import pandas as pd
    
    mean_spike_num = np.mean(bursts_spike_nums)
    mean_firing_rate = np.mean(bursts_FR)
    
    spike_nums = spike_nums / mean_spike_num
    firing_rates = firing_rates / mean_firing_rate
    
    return (spike_nums, firing_rates)
 
    
 
# two lists that store output data from all files for the cell    
spike_nums_global_list = []
firing_rates_global_list = []
   
for filename in files:
    
    path = foldername + filename
    
    import pyabf
    import numpy as np
    import pandas as pd

    fileabf = pyabf.ABF(path)
    fileabf.setSweep(sweepNumber=0, channel=0)
    IN0_mV = fileabf.sweepY

    spike_coords_list = mark_spikes(IN0_mV)

    (total_burst_number, bursts_spike_nums, bursts_FR) = detect_bursts(spike_coords_list)    
    (spike_nums, firing_rates) = group_bursts(total_burst_number, bursts_spike_nums, bursts_FR)
    (spike_nums, firing_rates) = normalize_to_mean(bursts_spike_nums, bursts_FR, spike_nums, firing_rates)
    
    # add pooled data from the file to the overall dataset for the current cell
    spike_nums_global_list.append(spike_nums)
    firing_rates_global_list.append(firing_rates)



# reformatted as dframes
spike_nums_global = pd.concat(spike_nums_global_list, ignore_index=True)
firing_rates_global = pd.concat(firing_rates_global_list, ignore_index=True)

# mean and median values
means_medians_spike_nums = pd.DataFrame(zip(spike_nums_global.mean(axis=0), spike_nums_global.median(axis=0)), columns=['mean', 'median'], index=['Mod','Control','Nonmod'])
means_medians_firing_rates = pd.DataFrame(zip(firing_rates_global.mean(axis=0), firing_rates_global.median(axis=0)), columns=['mean', 'median'], index=['Mod','Control','Nonmod'])


    

# plot everything
def plot_kde():
    import matplotlib.pyplot as plt  

    fig3 = plt.figure()
    plt.title(mix_type)

    extrm = 0
    gauss_bandwidth = 0.05 #0.02
    x_d = np.linspace(0, 3, 10000)
    #x_d = np.linspace(0, 10, 10000)
    x_d_T = x_d.reshape(-1, 1)

    from sklearn.neighbors import KernelDensity

    arr = np.array(spike_nums_global['Control'])
    arr = arr.reshape(-1, 1)
    kde = KernelDensity(bandwidth=gauss_bandwidth, kernel='gaussian').fit(arr)
    logprob = kde.score_samples(x_d_T)
    prob = np.exp(logprob)
    extrm = max(max(prob), extrm)

    plt.fill_between(x_d, prob, alpha=0.7, edgecolor='black', facecolor='white', label='control' + ' (n = ' + str(len(spike_nums_global['Control'])) + ')')
    #plt.plot(arr, np.full_like(arr, -0.01), '|k', markeredgewidth=0.7)


    arr = np.array(spike_nums_global['Mod'])
    arr = arr.reshape(-1, 1)
    kde = KernelDensity(bandwidth=gauss_bandwidth, kernel='gaussian').fit(arr)
    logprob = kde.score_samples(x_d_T)
    prob = np.exp(logprob)
    extrm = max(max(prob), extrm)

    plt.fill_between(x_d, prob, alpha=0.3, edgecolor='red', facecolor='red', label='mod' + ' (n = ' + str(len(spike_nums_global['Mod'])) + ')')



    arr = np.array(spike_nums_global['Nonmod'])
    arr = arr.reshape(-1, 1)
    kde = KernelDensity(bandwidth=gauss_bandwidth, kernel='gaussian').fit(arr)
    logprob = kde.score_samples(x_d_T)
    prob = np.exp(logprob)
    extrm = max(max(prob), extrm)

    plt.fill_between(x_d, prob, alpha=0.3, edgecolor='blue', facecolor='blue', label='nonmod' + ' (n = ' + str(len(spike_nums_global['Nonmod'])) + ')')


    plt.ylim(0, extrm + 1)
    plt.xlim(0, 3)
    #plt.xlim(1, 7)
    plt.xlabel('norm to mean spike count')
    plt.ylabel('kernel prob. density estimation')
    plt.legend()
    plt.show()





    # kde FR


    fig4 = plt.figure()
    plt.title(mix_type)

    extrm = 0
    gauss_bandwidth = 0.04 #0.5
    x_d = np.linspace(0, 3, 10000)
    #x_d = np.linspace(0, 40, 10000)
    x_d_T = x_d.reshape(-1, 1)

    from sklearn.neighbors import KernelDensity

    arr = np.array(firing_rates_global['Control'])
    arr = arr.reshape(-1, 1)
    kde = KernelDensity(bandwidth=gauss_bandwidth, kernel='gaussian').fit(arr)
    logprob = kde.score_samples(x_d_T)
    prob = np.exp(logprob)
    extrm = max(max(prob), extrm)

    plt.fill_between(x_d, prob, alpha=0.6, edgecolor='black', facecolor='white', label='control' + ' (n = ' + str(len(firing_rates_global['Control'])) + ')')
    #plt.plot(arr, np.full_like(arr, -0.01), '|k', markeredgewidth=0.7)


    arr = np.array(firing_rates_global['Mod'])
    arr = arr.reshape(-1, 1)
    kde = KernelDensity(bandwidth=gauss_bandwidth, kernel='gaussian').fit(arr)
    logprob = kde.score_samples(x_d_T)
    prob = np.exp(logprob)
    extrm = max(max(prob), extrm)

    plt.fill_between(x_d, prob, alpha=0.3, edgecolor='red', facecolor='red', label='mod' + ' (n = ' + str(len(firing_rates_global['Mod'])) + ')')



    arr = np.array(firing_rates_global['Nonmod'])
    arr = arr.reshape(-1, 1)
    kde = KernelDensity(bandwidth=gauss_bandwidth, kernel='gaussian').fit(arr)
    logprob = kde.score_samples(x_d_T)
    prob = np.exp(logprob)
    extrm = max(max(prob), extrm)

    plt.fill_between(x_d, prob, alpha=0.3, edgecolor='blue', facecolor='blue', label='nonmod' + ' (n = ' + str(len(firing_rates_global['Nonmod'])) + ')')


    plt.ylim(0, extrm + 0.05)
    plt.xlim(0, 3)
    #plt.xlim(10, 40)
    plt.xlabel('Norm to mean firing rate')
    plt.ylabel('kernel prob. density estimation')
    plt.legend()
    plt.show()



plot_kde()